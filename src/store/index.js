import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        dark_mode: false,
        favorites: []
    },

    mutations: {
        set_dark_mode: (state, data) => (state.dark_mode = data),
        set_favorites: (state, data) => (state.favorites = data),
    },

    actions: {
        SET_DARK_MODE: (context, data) => context.commit("set_dark_mode", data),
        SET_FAVORITES: (context, data) => context.commit("set_favorites", data),
    },

    getters: {
        GET_DARK_MODE: (state) => state.dark_mode,
        GET_FAVORITES: (state) => state.favorites,
    },
});
